.PHONY: clean

clean:
	@echo "Cleaning up..."
	find . -name "*~" -type f -delete
	@echo "Done."
