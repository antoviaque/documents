import os

from dotenv import load_dotenv

from langchain.prompts import (
    ChatPromptTemplate,
    MessagesPlaceholder,
    SystemMessagePromptTemplate,
    HumanMessagePromptTemplate
)
from langchain.chains import ConversationChain
from langchain.chat_models import ChatOpenAI
from langchain.memory import ConversationBufferMemory
from langchain.document_loaders.text import TextLoader

# Env variables #####################################################

load_dotenv()

# Main ##############################################################

prompt_text = """# Meeting Summary

## Definition

Produce a summary of a meeting, based on its raw transcript. Each prompt includes an excerpt from the raw transcript of the meeting. Reply to each excerpt with the summary of that excerpt.

Requirements for the summary:
* Include one or two paragraph(s) for each topic, summarizing the discussion and mentioning the name(s) of the people who speak
* IMPORTANT: The summary should be written in the past tense (`Jorge thanked the contributors`, and NOT `Jorge thanks the contributors`)
* Include titles before each topic summary (`## Title`)
* Format the result using markdown
"""
prompt = ChatPromptTemplate.from_messages([
    SystemMessagePromptTemplate.from_template(prompt_text),
    MessagesPlaceholder(variable_name="history"),
    HumanMessagePromptTemplate.from_template("{input}")
])

llm = ChatOpenAI(temperature=0, model_name="gpt-3.5-turbo-16k")
memory = ConversationBufferMemory(return_messages=True)
conversation = ConversationChain(memory=memory, prompt=prompt, llm=llm)

loader = TextLoader("/home/antoviaque/Documents/opencraft/community/core-sprints/meeting/openedx_contributors_meetup-20230711.txt")
excerpts = loader.load_and_split()

for excerpt in excerpts:
    print(conversation.predict(input=excerpt.page_content))
    print('\n')
